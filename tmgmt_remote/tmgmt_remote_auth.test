<?php
/**
 * @file
 * Base tests for RemoteAuth.
 *
 * To run tests in this file add following include into your module info file:
 * files[] = tmgmt_auth/tmgmt_auth.test
 */


class RemoteAuthTest extends DrupalWebTestCase {

  /**
   * Implements getInfo().
   */
  static function getInfo() {
    return array(
      'name' => 'TMGMTRemote tests',
      'description' => t('Tests basic remote auth logic'),
      'group' => 'TMGMT Client',
    );
  }

  function setUp() {
    parent::setUp(array('tmgmt_remote', 'tmgmt_auth_issuer_test', 'tmgmt_auth_receiver_test'));
  }

  /**
   * Tests if tables were properly created.
   */
  function testSchema() {

    $issued_keys_schema = tmgmt_auth_issuer_test_schema();
    $this->assertNotNull($issued_keys_schema['tmgmt_auth_issued_keys'],
      t('Issuer schema hook impl must contain tmgmt_auth_issued_keys table definition.'));
    $this->assertNotNull($issued_keys_schema['tmgmt_auth_tokens'],
      t('Issuer schema hook impl must contain tmgmt_auth_tokens table definition.'));


    $received_keys_schema = tmgmt_auth_receiver_test_schema();
    $this->assertNotNull($received_keys_schema['tmgmt_auth_received_keys'],
      t('Receiver schema hook impl must contain tmgmt_auth_received_keys table definition.'));

    $this->assertTrue(db_table_exists('tmgmt_auth_issued_keys'), t('Table tmgmt_auth_issued_keys created.'));
    $this->assertTrue(db_table_exists('tmgmt_auth_received_keys'), t('Table tmgmt_auth_received_keys created.'));
    $this->assertTrue(db_table_exists('tmgmt_auth_tokens'), t('Table tmgmt_auth_tokens created.'));
  }

  /**
   * Tests if entities were created properly.
   */
  function testEntity() {
    $issued_key_entity_info = entity_get_info('tmgmt_auth_issued_key');
    $this->assertNotNull($issued_key_entity_info, t('Entity info for tmgmt_auth_issued_key must exist.'));
    $this->assertEqual($issued_key_entity_info['module'], 'tmgmt_auth_issuer_test',
      t('Entity tmgmt_auth_issued_key must belong to the tmgmt_auth_issuer_test module.'));

    $token_entity_info = entity_get_info('tmgmt_auth_token');
    $this->assertNotNull($token_entity_info, t('Entity info for tmgmt_auth_token must exist.'));
    $this->assertEqual($token_entity_info['module'], 'tmgmt_auth_issuer_test',
      t('Entity tmgmt_auth_token must belong to the tmgmt_auth_issuer_test module.'));

    $received_key_entity_info = entity_get_info('tmgmt_auth_received_key');
    $this->assertNotNull($received_key_entity_info, t('Entity info for tmgmt_auth_received_key must exist.'));
    $this->assertEqual($received_key_entity_info['module'], 'tmgmt_auth_receiver_test',
      t('Entity tmgmt_auth_received_key must belong to the tmgmt_auth_receiver_test module.'));
  }

  /**
   * Test issue process.
   */
  function testIssueProcess() {
    /**
     * @var TMGMTAuthIssuedKeyController $issuer_controller
     */
    $issuer_controller = tmgmt_auth_issuer_controller();

    $issued_key = $issuer_controller->issue('mock_entity', 1);

    $issuer_controller->resetCache();

    // Load key by entity data and test values
    $loaded_issued_key = $issuer_controller->loadByEntity('mock_entity', 1);
    $this->assertEqual($loaded_issued_key->pub, $issued_key->pub);
    $this->assertEqual($loaded_issued_key->private, $issued_key->private);
    $this->assertEqual($loaded_issued_key->created, $issued_key->created);

    $issuer_controller->resetCache();

    // Load key by its pub and test values.
    $loaded_issued_key = $issuer_controller->loadByPub($issued_key->pub);
    $this->assertEqual($loaded_issued_key->private, $issued_key->private);
    $this->assertEqual($loaded_issued_key->created, $issued_key->created);

    // Try to issue a key for an entity with existing valid key.
    try {
      $issuer_controller->issue('mock_entity', 1);
      $this->fail(t('Issue process for an entity with existing valid key should have fail.'));
    }
    catch (TMGMTAuthIssuerException $e) {
      $this->pass(t('Issue process for an entity with existing valid key should fail.'));
    }

    $issuer_controller->revoke($loaded_issued_key);
    $this->assertNull($issuer_controller->loadByEntity('mock_entity', 1),
      t('Load by entity call should return only valid keys.'));

    $revoked_key = $issuer_controller->loadByPub($issued_key->pub);
    $this->assertEqual($revoked_key->private, $issued_key->private);
    $this->assertTrue($revoked_key->revoked > 0, t('Revoked value of the revoked key should be set to a timestamp value.'));
  }

  /**
   * Test authorization process.
   */
  function testAuthProcess() {
    /**
     * @var TMGMTAuthReceivedKeyController $receiver_controller
     */
    $receiver_controller = entity_get_controller('tmgmt_auth_received_key');
    /**
     * @var TMGMTAuthIssuedKeyController $issuer_controller
     */
    $issuer_controller = entity_get_controller('tmgmt_auth_issued_key');

    $keys = $this->doKeyExchange();
    $received_key = $keys['received_key'];

    $auth_token = $receiver_controller->createAuthToken($received_key);

    try {
      $issuer_controller->verifyAuthToken($auth_token);
      $this->pass(t('Valid auth token verification passed.'));
    }
    catch (TMGMTAuthIssuerException $e) {
      $this->fail(t('Valid auth token verification failed.') . $e->getMessage());
    }

    // Tamper the secret part.
    $tampered_auth_token = tmgmt_auth_string_parse($auth_token);
    $tampered_auth_token['secret'] .= 'e';
    $tampered_auth_token = tmgmt_auth_string_create(
      $tampered_auth_token['pub'], $tampered_auth_token['secret'], $tampered_auth_token['timestamp']);

    // Now do tmgmt_auth_MAX_AUTH_ATTEMPTS validations which will fail to
    // test if lockout occurs.
    for ($i = 0; $i < TMGMT_AUTH_MAX_ATTEMPTS; $i++) {
      try {
        $issuer_controller->verifyAuthToken($tampered_auth_token);
      }
      catch (TMGMTAuthIssuerException $e) {
        $this->assertEqual(t('Invalid authentication info.'), $e->getMessage(), t('%i. validation fail.', array('%i' => $i + 1)));
      }
    }

    // Do another validation with valid token but it should result in lockout
    // exception message as previous failed attempts were done with the
    // same key.
    try {
      $issuer_controller->verifyAuthToken($auth_token);
    }
    catch (TMGMTAuthIssuerException $e) {
      $this->assertEqual(t('Key temporarily blocked.'), $e->getMessage(), t('Validation failed due to lockout.'));
    }

    // Bypass the flood control to do further tests with the same key.
    flood_clear_event('tmgmt_auth_fail', $received_key->pub);
    $issuer_controller->revoke($keys['issued_key']);

    // Do validation with revoked key.
    try {
      $issuer_controller->verifyAuthToken($auth_token);
    }
    catch (TMGMTAuthIssuerException $e) {
      $this->assertEqual(t('Revoked key.'), $e->getMessage(), t('Validation failed due to revoked key.'));
    }

    // Bypass the flood control to do further tests with the same key.
    flood_clear_event('tmgmt_auth_fail', $received_key->pub);

    // Now update issued key in a way it will not be revoked but expired.
    $keys['issued_key']->revoked = 0;
    $keys['issued_key']->expired = REQUEST_TIME;
    $keys['issued_key']->last_request = 0;
    $issuer_controller->save($keys['issued_key']);
    $issuer_controller->resetCache();

    // Do validation with expired key.
    try {
      $issuer_controller->verifyAuthToken($auth_token);
    }
    catch (TMGMTAuthIssuerException $e) {
      $this->assertEqual(t('Expired key.'), $e->getMessage(), t('Validation failed due to expired key.'));
    }
  }

  /**
   * Test one-time token authorization process.
   */
  function testOneTimeTokenAuthProcess() {
    $keys = $this->doKeyExchange();
    $issued_key = $keys['issued_key'];
    $received_key = $keys['received_key'];

    /**
     * @var TMGMTAuthReceivedKeyController $receiver_controller
     */
    $receiver_controller = entity_get_controller('tmgmt_auth_received_key');
    /**
     * @var TMGMTAuthTokenController $token_controller
     */
    $token_controller = entity_get_controller('tmgmt_auth_token');
    /**
     * @var TMGMTAuthIssuedKeyController $issuer_controller
     */
    $issuer_controller = entity_get_controller('tmgmt_auth_issued_key');

    // Issue a token.
    $token = $token_controller->issue($issued_key->entity_type, $issued_key->entity_id, 'user', 1);

    // Try to issue another token for the same entity.
    try {
      $token_controller->issue($issued_key->entity_type, $issued_key->entity_id, 'user', 1);
      $this->fail(t('Multiple tokens for the same entity are not allowed.'));
    }
    catch (TMGMTAuthTokenException $e) {
      $this->pass(t('Token for an entity that has a valid token was not issued.'));
    }

    // Run verification routine for signed token. This should all pass.
    $signed_token = $receiver_controller->signToken($token->token, $received_key);
    $token_controller->verifySignedToken($signed_token);

    // Run verification for token that has been verified.
    try {
      $token_controller->verifySignedToken($signed_token);
      $this->fail(t('A validated token must not pass validation again.'));
    }
    catch (TMGMTAuthTokenException $e) {
      $this->pass(t('Revalidation of a validated token should fail.'));
    }

    // Issue new token and tamper it.
    $token = $token_controller->issue($issued_key->entity_type, $issued_key->entity_id, 'user', 1);
    $signed_token = $receiver_controller->signToken($token->token . 'e', $received_key);

    try {
      $token_controller->verifySignedToken($signed_token);
      $this->fail(t('Validation of a tampered token must not pass.'));
    }
    catch (TMGMTAuthTokenException $e) {
      $this->pass(t('Validation of a tampered token should fail.'));
    }

    // Now do tmgmt_auth_MAX_AUTH_ATTEMPTS more validations which will fail to
    // test if token is removed.
    for ($i = 0; $i < TMGMT_AUTH_MAX_ATTEMPTS; $i++) {
      try {
        $token_controller->verifySignedToken($signed_token);
      }
      catch (TMGMTAuthTokenException $e) {
        $this->pass(t('%i. validation fail.', array('%i' => $i + 2)));
      }
    }

    $this->assertNull($token_controller->loadByPub($token->pub),
      t('Token that failed validation %i times is killed.', array('%i' => TMGMT_AUTH_MAX_ATTEMPTS + 1)));

    // Revoke key and try to issue a token with that revoked key.
    $issuer_controller->revoke($issued_key);
    try {
      $token_controller->issue($issued_key->entity_type, $issued_key->entity_id, 'user', 1);
      $this->fail(t('Token must not be issued using revoked key.'));
    }
    catch (TMGMTAuthIssuerException $e) {
      $this->assertEqual($e->getMessage(), t('No valid key found.'), t('Token not issued due to revoked key.'));
    }

    // Now update the key to be expired and try to issue a token with it.
    $issued_key->revoked = 0;
    $issued_key->expires = REQUEST_TIME;
    $issuer_controller->save($issued_key);
    $issuer_controller->resetCache();
    try {
      $token_controller->issue($issued_key->entity_type, $issued_key->entity_id, 'user', 1);
      $this->fail(t('Token must not be issued using expired key.'));
    }
    catch (TMGMTAuthIssuerException $e) {
      $this->assertEqual($e->getMessage(), t('No valid key found.'), t('Token not issued due to expired key.'));
    }
  }

  function doKeyExchange() {

    $account1 = $this->drupalCreateUser();
    $account2 = $this->drupalCreateUser();

    /**
     * @var TMGMTAuthIssuedKeyController $issuer_controller
     */
    $issuer_controller = entity_get_controller('tmgmt_auth_issued_key');
    $issued_key = $issuer_controller->issue('user', $account1->uid);
    $key_info = array(
      'pub' => $issued_key->pub,
      'private' => $issued_key->private,
      'created' => $issued_key->created,
      'expires' => $issued_key->expires,
    );

    /**
     * @var TMGMTAuthReceivedKeyController $receiver_controller
     */
    $receiver_controller = entity_get_controller('tmgmt_auth_received_key');
    $receiver_controller->receive($key_info, 'user', $account2->uid);
    $received_key = $receiver_controller->loadByEntity('user', $account2->uid);

    // Test if values for issued key and received key are equal.
    $this->assertEqual($issued_key->pub, $received_key->pub);
    $this->assertEqual($issued_key->private, $received_key->private);
    $this->assertEqual($issued_key->expires, $received_key->expires);

    return array(
      'issued_key' => $issued_key,
      'received_key' => $received_key,
    );
  }
}
