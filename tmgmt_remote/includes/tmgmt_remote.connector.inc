<?php

/**
 * @file
 * TMGMT Directory connector.
 */

abstract class TMGMTRemoteConnector {
  /**
   * Base url.
   *
   * @var string
   */
  protected $base_url;

  /**
   * Additional params to pass with request.
   *
   * @var array
   */
  protected $qParams = array();

  protected $postParams = array();
  protected $responseMessages = array();
  protected $headers = array();
  protected $responseCode;
  protected $originalResponse;


  /**
   * @param string $base_url
   *   Remote server base URL.
   */
  function __construct($base_url) {
    $this->base_url = rtrim($base_url, '/');
    $this->addHeader('User-Agent', $this->getUserAgent());
  }

  /**
   * Adds query parameter.
   *
   * @param string $key
   * @param string $value
   */
  function addQueryParam($key, $value) {
    $this->qParams[$key] = $value;
  }

  function addPostParam($key, $value) {
    $this->postParams[$key] = $value;
  }

  /**
   * Gets messages that came in with response.
   *
   * @return array
   *   List of messages.
   */
  function getResponseMessages() {
    return $this->responseMessages;
  }

  /**
   * Gets latest response code.
   *
   * @return int
   */
  function getResponseCode() {
    return $this->responseCode;
  }

  function getOriginalResponse() {
    return $this->originalResponse;
  }

  /**
   * Add header value.
   *
   * @param string $key
   * @param string $value
   */
  function addHeader($key, $value) {
    $this->headers[$key] = $value;
  }

  /**
   * In case you want to reuse an existing instance this method will reset
   * all previously set query, post and header params.
   */
  function reset() {
    $this->qParams = $this->postParams = $this->headers = array();
  }

  abstract function authenticate($resource, array $options = array());

  abstract function getEndPoint();

  function getUserAgent() {

    $path = drupal_get_path('module', 'system') . '/system.info';
    $info = drupal_parse_info_file($path);
    $drupal_version = $info['version'];

    $path = drupal_get_path('module', 'tmgmt') . '/tmgmt.info';
    $info = drupal_parse_info_file($path);
    $tmgmt_version = isset($info['version']) ? $info['version'] : 'dev';

    return 'Drupal/' . $drupal_version . ' TMGMT/' . $tmgmt_version;
  }

  public function getTargetUrl($resource) {
    return url($this->base_url . '/' . $this->getEndPoint() . '/' . $resource, array('query' => $this->qParams));
  }

  /**
   * Gets response from DS.
   *
   * @param string $resource
   *   Resource to connect to.
   * @param array $options
   *   Additional options passed into drupal_http_request().
   *
   * @return array
   *   Data received from DS.
   *
   * @throws TMGMTRemoteConnectionException
   *   - In case of connection error.
   * @throws TMGMTRemoteValidationException
   *   - In case remote server sends error in the response.
   */
  protected function getResponse($resource, $options = array()) {

    if (empty($this->base_url)) {
      throw new TMGMTRemoteConnectionException('Missing target service url.');
    }

    $this->authenticate($resource, $options);
    $url = $this->getTargetUrl($resource);

    // In case we have postParams set needed request data.
    if (!empty($this->postParams)) {
      $this->headers['Content-type'] = 'application/x-www-form-urlencoded';
      $options['method'] = 'POST';
      $options['data'] = drupal_http_build_query($this->postParams);
    }

    if (!empty($this->headers)) {
      $options['headers'] = $this->headers;
    }

    $response = drupal_http_request($url, $options);

    $this->originalResponse = $response;
    $this->responseCode = $response->code;

    if ($response->code != 200) {
      throw new TMGMTRemoteConnectionException('Unable to connect to the remote service due to following error: @error at @url',
        array('@error' => $response->error, '@url' => $url));
    }

    if (empty($response->data)) {
      throw new TMGMTRemoteValidationException('Remote server responded with empty response at @url.', array('@url' => $url));
    }

    $response = drupal_json_decode($response->data);

    $this->responseMessages = array();

    if (isset($response['messages'])) {
      $this->responseMessages = $response['messages'];
    }

    if ($response['status'] == 'ok') {
      return isset($response['data']) ? $response['data'] : NULL;
    }

    if (empty($response['messages']['error'])) {
      throw new TMGMTRemoteValidationException('Remote server responded with unknown error.');
    }

    $messages = NULL;
    foreach ($response['messages']['error'] as $code => $_messages) {
      $messages .= t('Server responded with error %code: %message',
        array('%code' => $code, '%message' => implode(' | ', $_messages)));
    }

    throw new TMGMTRemoteValidationException($messages);
  }
}

class TMGMTRemoteGenericConnector extends TMGMTRemoteConnector {

  public $entity_info = array();
  private $local_endpoint = 'api/v1';
  private $responseErrror;

  function getUserAgent() {
    return parent::getUserAgent() . ' GenericConnector';
  }

  function getEndPoint() {
    return $this->local_endpoint;
  }

  function setEndPoint($endpoint) {
    $this->local_endpoint = $endpoint;
  }

  function setEntityInfo($entity_type = NULL, $entity_id = NULL) {
    $this->entity_info = array(
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
    );
  }

  function authenticate($resource, array $options = array()) {

    // If entity info is not set we will not authenticate.
    if (empty($this->entity_info['entity_type']) || empty($this->entity_info['entity_id'])) {
      return;
    }

    $key = tmgmt_auth_receiver_controller()
        ->loadByEntity($this->entity_info['entity_type'], $this->entity_info['entity_id']);
    $this->addHeader('Authenticate', tmgmt_auth_receiver_controller()->createAuthToken($key));
  }

  function get($resource, array $query_params = array()) {
    $this->reset();

    foreach ($query_params as $key => $value) {
      $this->addQueryParam($key, $value);
    }

    try {
      return $this->getResponse($resource);
    }
    catch (Exception $e) {
      $this->responseErrror = $e->getMessage();
      debug($e->getMessage());
      return FALSE;
    }
  }

  function post($resource, array $data) {
    $this->reset();

    foreach ($data as $key => $value) {
      $this->addPostParam($key, $value);
    }

    try {
      return $this->getResponse($resource);
    }
    catch (Exception $e) {
      $this->responseErrror = $e->getMessage();
      debug($e->getMessage());
      return FALSE;
    }
  }

  function delete($resource) {
    $this->reset();

    try {
      return $this->getResponse($resource, array('method' => 'DELETE'));
    }
    catch (Exception $e) {
      $this->responseErrror = $e->getMessage();
      debug($e->getMessage());
      return FALSE;
    }
  }

  function getResponseError() {
    return $this->responseErrror;
  }
}
