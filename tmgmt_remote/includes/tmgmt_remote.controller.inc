<?php
/**
 * @file
 * Controller classes for RemoteAuth library.
 */

/**
 * Controller acting as very simple CA with the ability to authenticate clients.
 */
class TMGMTAuthIssuedKeyController extends EntityAPIController {

  /**
   * Entity object that has been successfully authenticated by last
   * verifyAuthToken() call.
   *
   * @var Entity
   */
  protected $authenticatedEntity = NULL;

  function create(array $values = array()) {
    $values['created'] = time();
    return parent::create($values);
  }

  /**
   * Will issue a key for provided entity.
   *
   * @param string $entity_type
   *   Drupal entity type representing the client system.
   * @param $entity_id
   *   Drupal entity id representing the client system.
   *
   * @return TMGMTAuthIssuedKey
   *   Newly created and loaded key entity.
   * @throws TMGMTAuthIssuerException
   *   In case a valid key for provided entity exists.
   */
  function issue($entity_type, $entity_id) {

    $existing = $this->loadByEntity($entity_type, $entity_id);

    if (!empty($existing)) {
      throw new TMGMTAuthIssuerException(t('Cannot issue key for provided entity. A valid key already exists.'));
    }

    do {
      $pub = user_password(TMGMT_AUTH_KEY_LENGTH);
    }
    while ($this->loadByPub($pub));

    $values = array(
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'pub' => $pub,
      'private' => user_password(TMGMT_AUTH_KEY_LENGTH),
    );

    $issued_key = $this->create($values);
    $this->save($issued_key);

    // Return loaded entity from db so that all db default vals are present.
    return $this->loadByPub($issued_key->pub);
  }

  /**
   * Will revoke passed in key.
   *
   * @param TMGMTAuthIssuedKey $issued_key
   *   Key entity to be revoked.
   */
  function revoke(TMGMTAuthIssuedKey $issued_key) {
    $issued_key->revoked = time();
    $this->save($issued_key);
    $this->resetCache(array($issued_key->key_id));
  }

  /**
   * Will revoke key with provided public identifier.
   *
   * @param string $pub
   *   Public identifier of a key entity.
   */
  function revokeByPub($pub) {
    $key = $this->loadByPub($pub);
    $this->revoke($key);
  }

  /**
   * Determines if provided key is valid.
   *
   * @param TMGMTAuthIssuedKey $issued_key
   *   Key for which to check validity.
   *
   * @return bool
   *   Check result.
   */
  function isValid(TMGMTAuthIssuedKey $issued_key) {
    return (
        ($issued_key->expires > time() || $issued_key->expires == 0) &&
            ($issued_key->revoked == 0)
    );
  }

  /**
   * Loads key entity by its pub.
   *
   * Note that this method loads the key without validation check and therefore
   * it is callers responsibility to check key validity prior to doing
   * operations requiring a valid key.
   *
   * @param string $pub
   *   Public key identifier.
   *
   * @return TMGMTAuthIssuedKey
   *   Loaded key entity.
   */
  function loadByPub($pub) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'tmgmt_auth_issued_key');
    $query->propertyCondition('pub', $pub);
    $result = $query->execute();
    if (isset($result['tmgmt_auth_issued_key']) && !empty($result['tmgmt_auth_issued_key'])) {
      $result = array_keys($result['tmgmt_auth_issued_key']);
      return entity_load_single('tmgmt_auth_issued_key', array_shift($result));
    }

    return NULL;
  }

  /**
   * Loads key entity for provided entity.
   *
   * This method will return only valid key, which is not expired and not
   * revoked.
   *
   * @param string $entity_type
   *   Drupal entity type.
   * @param int $entity_id
   *   Drupal entity id.
   *
   * @return TMGMTAuthIssuedKey
   *   Key entity.
   */
  function loadByEntity($entity_type, $entity_id) {
    $query = db_select('tmgmt_auth_issued_keys', 'k');
    $query->addField('k', 'key_id');
    $query->condition('entity_type', $entity_type);
    $query->condition('entity_id', $entity_id);
    $query->condition('revoked', 0);
    $query->condition(db_or()->condition('expires', time(), '>')->condition('expires', 0));

    if ($id = $query->execute()->fetchField()) {
      return entity_load_single('tmgmt_auth_issued_key', $id);
    }

    return NULL;
  }

  /**
   * Loads all existing keys issued for the entity.
   *
   * @param string $entity_type
   *   Drupal entity type.
   * @param int $entity_id
   *   Drupal entity id.
   *
   * @return array
   *   Array of TMGMTAuthIssuedKey entities.
   */
  function loadAllByEntity($entity_type, $entity_id) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'tmgmt_auth_issued_key');
    $query->propertyCondition('entity_type', $entity_type);
    $query->propertyCondition('entity_id', $entity_id);
    $result = $query->execute();

    if (isset($result['tmgmt_auth_issued_key']) && !empty($result['tmgmt_auth_issued_key'])) {
      return entity_load('tmgmt_auth_issued_key', array_keys($result['tmgmt_auth_issued_key']));
    }

    return array();
  }

  /**
   * Authorization routine.
   *
   * The routine performs following operations:
   * - Checks if a key with provided public identifier exists.
   * - Checks if the key is revoked.
   * - Checks if the key has expired.
   * - Checks if the provided public identifier was involved in failed auth
   *   attempts. If it exceeds REMOTE_AUTH_MAX_AUTH_ATTEMPTS it will be locked
   *   for REMOTE_AUTH_FAILED_AUTH_LOCK_TIME.
   * - Checks if provided timestamp is not in past compared to last request
   *   timestamp of the given key.
   * - Checks if auth token is valid.
   *
   * @param string $auth_token
   *   Auth token string in format produced by tmgmt_auth_string_create().
   *
   * @throws TMGMTAuthIssuerException
   *   In case of validation error.
   * @throws TMGMTAuthMalformedAuthStringException
   *   In case of malformed auth_token string.
   */
  function verifyAuthToken($auth_token) {

    // Wipe out any previously authenticated entity.
    $this->authenticatedEntity = NULL;

    $auth_info = tmgmt_auth_string_parse($auth_token);

    $key = $this->loadByPub($auth_info['pub']);

    if (empty($key)) {
      throw new TMGMTAuthIssuerException(t('Unknown key.').$auth_info['pub']);
    }

    if ($key->revoked) {
      throw new TMGMTAuthIssuerException(t('Revoked key.'));
    }

    if (!empty($key->expires) && $key->expires < time()) {
      throw new TMGMTAuthIssuerException(t('Expired key.'));
    }

    if (!flood_is_allowed('tmgmt_auth_fail', TMGMT_AUTH_MAX_ATTEMPTS, TMGMT_AUTH_FAILED_LOCK_TIME, $auth_info['pub'])) {
      throw new TMGMTAuthIssuerException(t('Key temporarily blocked.'));
    }

    if (!is_numeric($auth_info['timestamp']) || $key->last_request >= $auth_info['timestamp']) {
      flood_register_event('tmgmt_auth_fail', TMGMT_AUTH_FAILED_LOCK_TIME, $auth_info['pub']);
      throw new TMGMTAuthIssuerException(t('Invalid authentication info.'));
    }

    if (tmgmt_auth_token_sign($auth_info['timestamp'], $key->private) != $auth_info['secret']) {
      flood_register_event('tmgmt_auth_fail', TMGMT_AUTH_FAILED_LOCK_TIME, $auth_info['pub']);
      throw new TMGMTAuthIssuerException(t('Invalid authentication info.'));
    }

    $key->last_request = $auth_info['timestamp'];
    $this->save($key);
    $this->resetCache(array($key->key_id));

    flood_clear_event('tmgmt_auth_fail', $auth_info['pub']);

    $this->authenticatedEntity = $this->loadEntityByPub($auth_info['pub']);
  }

  /**
   * Helper method to load entity associated with a key.
   *
   * @param string $pub
   *   Key public identifier.
   * @return object
   *   Entity object.
   *
   * @throws TMGMTAuthIssuerException
   *   In case a key with provided public identifier does not exists.
   */
  function loadEntityByPub($pub) {
    $key = $this->loadByPub($pub);

    if (empty($key)) {
      throw new TMGMTAuthIssuerException(t('Unknown key.'));
    }

    return entity_load_single($key->entity_type, $key->entity_id);
  }

  /**
   * Gets last authenticated entity.
   *
   * @return Entity
   */
  function getAuthenticatedEntity() {
    return $this->authenticatedEntity;
  }

}

/**
 * Controller with authorization client responsibilities.
 */
class TMGMTAuthReceivedKeyController extends EntityAPIController {

  /**
   * Saves key entity based on provided data.
   *
   * Note that there can be only one valid key for a specific remote server
   * entity, so in case of duplicate it throws an exception. In case it is
   * intended to replace the existing key, switch the $force_replace flag
   * to TRUE.
   *
   * @param array $key_info
   *   Auth string in the format produced by tmgmt_auth_string_create().
   * @param string $entity_type
   *   Drupal entity type representing the server system.
   * @param int $entity_id
   *   Drupal entity id representing the server system.
   * @param boolean $force_replace
   *   Flag to force the replace of older key received for the very same entity.
   *
   * @return TMGMTAuthReceivedKey
   *   Newly created and loaded key entity.
   * @throws TMGMTAuthReceiverException
   *   In case of the attempt to receive a key for a remote server entity which
   *   already has a valid key.
   */
  function receive(array $key_info, $entity_type, $entity_id, $force_replace = FALSE) {

    if (strlen($key_info['pub']) != TMGMT_AUTH_KEY_LENGTH || strlen($key_info['private']) != TMGMT_AUTH_KEY_LENGTH) {
      throw new TMGMTAuthReceiverException(t('Invalid key length.'));
    }
    if (!is_numeric($key_info['created'])) {
      throw new TMGMTAuthReceiverException(t('Invalid created info.'));
    }
    if (!is_numeric($key_info['expires'])) {
      throw new TMGMTAuthReceiverException(t('Invalid expires info.'));
    }

    $existing_entity = $this->loadByEntity($entity_type, $entity_id);

    if ($force_replace && !empty($existing_entity)) {
      $this->delete(array($existing_entity->key_id));
    }
    elseif (!empty($existing_entity)) {
      throw new TMGMTAuthReceiverException(t('A valid key exists for provided remote server entity.'));
    }

    $received_key = $this->create(array(
      'pub' => $key_info['pub'],
      'private' => $key_info['private'],
      'created' => $key_info['created'],
      'expires' => $key_info['expires'],
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
    ));

    $this->save($received_key);

    // Load the entity from db to pick up db default values.
    return $this->loadByEntity($entity_type, $entity_id);
  }

  /**
   * Loads key by the remote server entity.
   *
   * Note that it will return only valid key, that is which has not expired yet.
   *
   * @param string $entity_type
   *   Drupal entity type of remote server.
   * @param $entity_id
   *   Drupal entity id of remote server.
   *
   * @return TMGMTAuthReceivedKey
   *   Key entity.
   */
  function loadByEntity($entity_type, $entity_id) {
    $query = db_select('tmgmt_auth_received_keys', 'k');
    $query->addField('k', 'key_id');
    $query->condition('entity_type', $entity_type);
    $query->condition('entity_id', $entity_id);
    $query->condition(db_or()->condition('expires', time(), '>')->condition('expires', 0));

    if ($id = $query->execute()->fetchField()) {
      return entity_load_single('tmgmt_auth_received_key', $id);
    }

    return NULL;
  }

  /**
   * Loads all existing keys received for the entity.
   *
   * @param string $entity_type
   *   Drupal entity type.
   * @param int $entity_id
   *   Drupal entity id.
   *
   * @return array
   *   Array of TMGMTAuthReceivedKey entities.
   */
  function loadAllByEntity($entity_type, $entity_id) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'tmgmt_auth_received_key');
    $query->propertyCondition('entity_type', $entity_type);
    $query->propertyCondition('entity_id', $entity_id);
    $result = $query->execute();

    if (isset($result['tmgmt_auth_received_key']) && !empty($result['tmgmt_auth_received_key'])) {
      return entity_load('tmgmt_auth_received_key', array_keys($result['tmgmt_auth_received_key']));
    }

    return array();
  }

  /**
   * Creates a auth token to be sent to the remote system for authentication.
   *
   * @param TMGMTAuthReceivedKey $key
   *   Key with which to create the auth token.
   *
   * @return string
   *   Auth token in tmgmt_auth_string_create() format.
   */
  function createAuthToken(TMGMTAuthReceivedKey $key) {
    $utime = tmgmt_remote_microtime();

    $secret = tmgmt_auth_token_sign($utime, $key->private);
    return tmgmt_auth_string_create($key->pub, $secret, $utime);
  }

  /**
   * Signs received token to do transactional one-time authorization.
   *
   * You can use this feature to authorize an intention of the client system
   * to perform an action. Or use it to verify third party clients trusted by
   * a CA that can issue such token and verify its validity.
   *
   * @param string $token
   *   Token string to be signed.
   * @param TMGMTAuthReceivedKey $key
   *   Key with which to sign the token.
   *
   * @return string
   *   Auth token in the tmgmt_auth_string_create() format.
   */
  function signToken($token, TMGMTAuthReceivedKey $key) {
    $secret = tmgmt_auth_token_sign($token, $key->private);
    return tmgmt_auth_string_create($key->pub, $secret, time());
  }
}

/**
 * Controller class to manage one-time authorization tokens.
 */
class TMGMTAuthTokenController extends EntityAPIController {

  function create(array $values = array()) {
    $values += array(
      'created' => time(),
      'expires' => time() + TMGMT_AUTH_TOKEN_LIFE_TIME,
    );
    return parent::create($values);
  }

  /**
   * Issues a token to be signed by remote client system.
   *
   * @param string $entity_type
   *   Drupal entity type of a remote client.
   * @param int $entity_id
   *   Drupal entity id of a remote client
   * @param string $target_entity_type
   *   Drupal entity type of a target object.
   * @param int $target_entity_id
   *   Drupal entity id of a target object.
   *
   * @return TMGMTAuthToken
   *   Newly created and loaded token entity.
   * @throws TMGMTAuthIssuerException
   *   In case no valid key found for the client entity.
   * @throws TMGMTAuthTokenException
   *   In case there is a valid token for provided entity.
   */
  function issue($entity_type, $entity_id, $target_entity_type, $target_entity_id) {
    /**
     * @var TMGMTAuthIssuedKeyController $issuer_controller
     */
    $issuer_controller = entity_get_controller('tmgmt_auth_issued_key');
    $key = $issuer_controller->loadByEntity($entity_type, $entity_id);

    if (empty($key)) {
      throw new TMGMTAuthIssuerException(t('No valid key found.'));
    }

    $existing_token = $this->loadByPub($key->pub);

    if (!empty($existing_token)) {
      throw new TMGMTAuthTokenException(t('Token for provided entity exists.'));
    }

    // Remove old token if any.
    $expired_token = $this->loadByPub($key->pub, FALSE);
    if (!empty($expired_token)) {
      $this->delete(array($expired_token->token_id));
    }

    $token = $this->create(array(
      'pub' => $key->pub,
      'token' => user_password(TMGMT_AUTH_KEY_LENGTH),
      'target_entity_type' => $target_entity_type,
      'target_entity_id' => $target_entity_id,
    ));

    $this->save($token);

    return $this->loadByPub($key->pub);
  }

  /**
   * Loads token based on provided public identifier of a key.
   *
   * @param string $pub
   *   Public identifier of a key.
   * @param boolean $check_expired
   *   Flag to determine if to check expired time of the token.
   *
   * @return TMGMTAuthToken
   *   Token entity.
   */
  function loadByPub($pub, $check_expired = TRUE) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'tmgmt_auth_token');
    $query->propertyCondition('pub', $pub);

    if ($check_expired) {
      $query->propertyCondition('expires', time(), '>');
    }

    $result = $query->execute();

    if (isset($result['tmgmt_auth_token']) && !empty($result['tmgmt_auth_token'])) {
      $result = array_keys($result['tmgmt_auth_token']);
      return entity_load_single('tmgmt_auth_token', array_shift($result));
    }

    return NULL;
  }

  /**
   * Signed token verification routine.
   *
   * The verification routine runs these operations:
   * - tmgmt_auth_string_parse performs auth token string format checks.
   * - Checks if there is a token for the public key identifier.
   * - Checks if token has not expired yet.
   * - Checks if # of auth attempts do not exceed REMOTE_AUTH_MAX_AUTH_ATTEMPTS.
   * - Checks token signature
   *
   * Token object is removed from database if:
   * - Validation passed
   * - Validation failed due to inability to verify signature three times in a
   *   row.
   * - Token is expired.
   *
   * @param string $signed_token
   *   Auth string in the tmgmt_auth_string_create().
   *
   * @throws TMGMTAuthTokenException
   *   In case of validation error.
   * @throws TMGMTAuthMalformedAuthStringException
   *   In case of malformed $signed_token auth string.
   */
  function verifySignedToken($signed_token) {
    $token_info = tmgmt_auth_string_parse($signed_token);
    $token = $this->loadByPub($token_info['pub']);

    if (empty($token)) {
      throw new TMGMTAuthTokenException(t('Unknown token.'));
    }

    // If token has expired, fail validation and kill it.
    if ($token->expires < time()) {
      $this->delete(array($token->token_id));
      throw new TMGMTAuthTokenException(t('Token expired.'));
    }

    // If verification failed three times we can be sure requests are
    // suspicious and so kill the token.
    if (!flood_is_allowed('remote_token_fail', TMGMT_AUTH_MAX_ATTEMPTS, TMGMT_AUTH_FAILED_LOCK_TIME, $token_info['pub'])) {
      $this->delete(array($token->token_id));
      throw new TMGMTAuthTokenException(t('Token verification failed @i times in a row. Token destroyed.',
        array('@i' => TMGMT_AUTH_MAX_ATTEMPTS + 1)));
    }

    /**
     * @var TMGMTAuthIssuedKeyController $issuer_controller
     */
    $issuer_controller = entity_get_controller('tmgmt_auth_issued_key');

    // At this moment we do not care about key validity as at the time
    // token was created the key was valid.
    $key = $issuer_controller->loadByPub($token_info['pub']);

    // Calculate secret based on token value and private key.
    $secret = tmgmt_auth_token_sign($token->token, $key->private);

    if ($secret != $token_info['secret']) {
      flood_register_event('remote_token_fail', TMGMT_AUTH_FAILED_LOCK_TIME, $token_info['pub']);
      throw new TMGMTAuthTokenException(t('Token verification failed.'));
    }

    // Upon successful verification delete the token.
    $this->delete(array($token->token_id));
  }
}
