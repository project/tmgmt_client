<?php

/**
 * TMGMT remote response class.
 *
 * array(
 *   'status' => 'ok/error',
 *   'data' => mixed,
 *   'messages' => array(
 *     'error' => array(),
 *     'ok' => array(),
 *   ),
 * )
 */
class TMGMTResponse {

  /**
   * Response sent back to the client.
   *
   * @var array
   */
  protected $response = array();

  function __construct() {
    // Set ok status as default.
    $this->response['status'] = 'ok';
  }

  function setEventCode($code, $message = NULL) {
    if (empty($message)) {
      $message = tmgmt_remote_response_messages($code);
    }
    $this->response['messages']['ok'][$code][] = $message;
  }

  function setErrorCode($code, $message = NULL) {
    $this->response['status'] = 'error';

    if (empty($message)) {
      $message = tmgmt_remote_response_messages($code);
    }
    $this->response['messages']['error'][$code][] = $message;
  }

  /**
   * Gets response messages.
   *
   * @return array
   */
  function getMessages() {
    return $this->response['messages'];
  }

  /**
   * Sets response data.
   *
   * Each call will override previously set data.
   *
   * @param mixed $data
   *   Data we want to send back in the response.
   */
  function setData($data) {
    $this->response['data'] = $data;
  }

  /**
   * Builds and returns response to be sent to a client.
   *
   * @return array
   *   Response array.
   */
  function getResponse() {
    return $this->response;
  }

  /**
   * Helper to easier handle the local logic in service callbacks.
   *
   * @return bool
   */
  function isError() {
    return $this->response['status'] == 'error';
  }
}

function tmgmt_remote_response_messages($code) {

  $messages = module_invoke_all('tmgmt_response_messages');

  if (!isset($messages[$code])) {
    throw new TMGMTRemoteException('Undefined message code: @code', array('@code' => $code));
  }
  return $messages[$code];
}
