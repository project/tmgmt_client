<?php

/**
 * Drupal entity representing issued key.
 */
class TMGMTAuthIssuedKey extends Entity {
  public $key_id;
  public $pub;
  public $private;
  public $created;
  public $expires;
  public $last_request;
  public $revoked;
  public $entity_type;
  public $entity_id;
}

/**
 * Drupal entity representing received key.
 */
class TMGMTAuthReceivedKey extends Entity {
  public $key_id;
  public $pub;
  public $private;
  public $created;
  public $expires;
  public $entity_type;
  public $entity_id;
}

/**
 * Drupal entity representing issued token.
 */
class TMGMTAuthToken extends Entity {
  public $token_id;
  public $pub;
  public $target_entity_type;
  public $target_entity_id;
  public $token;
  public $created;
  public $expires;
}
