<?php
/**
 * @file
 *   tmgmt_client controller classes.
 */

/**
 * TMGMTRemoteServer entity controller
 */
class TMGMTRemoteServerController extends EntityAPIController {

  /**
   * Loads remote server entity by its url.
   *
   * @param string $url
   *
   * @return TMGMTRemoteServer
   */
  function loadByUrl($url) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'tmgmt_remote_server');
    $query->propertyCondition('url', $url);
    $result = $query->execute();

    if (isset($result['tmgmt_remote_server'])) {
      $result = array_keys($result['tmgmt_remote_server']);
      return entity_load_single('tmgmt_remote_server', array_shift($result));
    }

    return NULL;
  }

  /**
   * Loads all servers.
   *
   * @param string $bundle
   * @param string $key_by
   *
   * @return array
   *   List of TMGMTRemoteServer entities.
   */
  function loadAll($bundle = 'ts', $key_by = 'server_id') {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'tmgmt_remote_server');
    $query->propertyCondition('type', $bundle);
    $result = $query->execute();

    if (isset($result['tmgmt_remote_server'])) {
      $result = array_keys($result['tmgmt_remote_server']);

      if ($key_by != 'server_id') {
        $servers = array();
        foreach (entity_load('tmgmt_remote_server', $result) as $server) {
          $servers[$server->{$key_by}] = $server;
        }
        return $servers;
      }

      return entity_load('tmgmt_remote_server', $result);
    }

    return array();
  }

  function delete($ids, DatabaseTransaction $transaction = NULL) {

    foreach ($ids as $id) {
      $keys = tmgmt_auth_receiver_controller()->loadAllByEntity('tmgmt_remote_server', $id);
      tmgmt_auth_receiver_controller()->delete(array_keys($keys));
    }

    parent::delete($ids, $transaction);
  }
}
