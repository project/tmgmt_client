<?php
/**
 * @file
 *   tmgmt_client forms and form handlers.
 */


/**
 * Entry point for the DS configuration workflow.
 *
 * This form is displayed as a checkout settings form at the job checkout.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 * @param TMGMTJob $job
 *   Job entity that is being checked out.
 *
 * @return array
 *   Drupal form array.
 */
function tmgmt_client_ds_checkout_form($form, &$form_state, TMGMTJob $job) {

  $ds_url = $job->getTranslator()->getSetting('ds_url');
  $server_id = $job->getSetting('server_id');
  $directory_server = NULL;

  if (!empty($ds_url)) {
    $directory_server = tmgmt_client_remote_server_controller()->loadByUrl($ds_url);
  }
  if (!empty($server_id)) {
    $translation_server = entity_load_single('tmgmt_remote_server', $server_id);
  }
  if (!empty($translation_server)) {
    $ts_key = tmgmt_auth_receiver_controller()->loadByEntity('tmgmt_remote_server', $translation_server->server_id);
  }

  if (empty($ds_url) || empty($directory_server)) {
    return tmgmt_client_ds_checkout_authenticate_form($form, $form_state, $job);
  }
  elseif (empty($server_id)) {
    return tmgmt_client_ds_checkout_select_ts_form($form, $form_state, $job);
  }
  elseif (empty($ts_key)) {
    return tmgmt_client_ds_checkout_negotiate_ts_key($form, $form_state, $job);
  }
  else {

    $translation_server = entity_load_single('tmgmt_remote_server', $server_id);

    $form['server_name'] = array(
      '#markup' => '<strong>' .
          t('Selected Translation Server: %name', array('%name' => $translation_server->name)) . '</strong>',
    );
    $form['all_configured'] = array(
      '#markup' => '<div class="messages status">' . t('All configured, proceed by submitting the job.') . '</div>'
    );
    $form['job_comment'] = array(
      '#type' => 'textarea',
      '#title' => t('Job comment'),
      '#description' => t('Provide additional info or instructions which can be useful for the translator.'),
      '#default_value' => $job->getSetting('job_comment'),
    );
    $form['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#submit' => array('tmgmt_client_ds_checkout_cancel_submit'),
      '#ajax' => array(
        'callback' => 'tmgmt_client_checkout_form_ajax',
        'wrapper' => 'tmgmt-ui-translator-settings',
      ),
    );
  }

  return $form;
}

function tmgmt_client_ds_checkout_cancel_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form_state['tmgmt_job']->settings['server_id'] = NULL;
  $form_state['tmgmt_job']->save();
}

/**
 * Form to trigger credentials negotiation.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 * @param TMGMTJob $job
 *   Job entity that is being checked out.
 *
 * @return array
 *   Drupal form array.
 */
function tmgmt_client_ds_checkout_negotiate_ts_key($form, &$form_state, $job) {

  $translation_server = entity_load_single('tmgmt_remote_server', $job->getSetting('server_id'));

  $form['server_name'] = array(
    '#markup' => '<strong>' .
        t('Selected Translation Server: %name', array('%name' => $translation_server->name)) . '</strong>',
  );
  $form['key_info'] = array(
    '#markup' => '<div class="messages warning">' .
        t('You do not have a key for the selected Translation Server. Press the "Get Translation Server key" button to order the TMGMT Directory to mediate the key for you.') . '</div>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get Translation Server key'),
    '#validate' => array('tmgmt_client_ds_checkout_negotiate_ts_key_validate'),
    '#submit' => array('tmgmt_client_ds_checkout_negotiate_ts_key_submit'),
    '#ajax' => array(
      'callback' => 'tmgmt_client_checkout_form_ajax',
      'wrapper' => 'tmgmt-ui-translator-settings',
    ),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('tmgmt_client_ds_checkout_cancel_submit'),
    '#ajax' => array(
      'callback' => 'tmgmt_client_checkout_form_ajax',
      'wrapper' => 'tmgmt-ui-translator-settings',
    ),
  );
  return $form;
}

/**
 * Validate callback for tmgmt_client_ds_config_negotiate_ts_key_form().
 *
 * Does communication with DS and TS to finally receive TS key which stored in
 * form state for further processing in the submit callback.
 *
 * @see tmgmt_client_checkout_settings_negotiate_ts_key_form_submit()
 * @see tmgmt_client_checkout_settings_negotiate_ts_key_form()
 */
function tmgmt_client_ds_checkout_negotiate_ts_key_validate($form, &$form_state) {
  /**
   * @var TMGMTJob $job
   */
  $job = $form_state['tmgmt_job'];
  $ds_url = $job->getTranslator()->getSetting('ds_url');
  $ds_connector = new TMGMTClientDSConnector($ds_url);
  /**
   * @var TMGMTRemoteServer $translation_server
   */
  $translation_server = entity_load_single('tmgmt_remote_server', $job->getSetting('server_id'));

  // Init credentials negotiation by contacting DS and obtaining a one time
  // auth token.
  try {
    $response = $ds_connector->createVerificationToken($translation_server->external_id);
  }
  catch (TMGMTRemoteException $e) {
    watchdog('tmgmt_client', 'Create verification token error: %error', array('%error' => $e->getMessage()), WATCHDOG_ERROR);
    form_set_error('', $e->getMessage());
    return;
  }

  // Sign the token and pass it over to the TS.
  $key = tmgmt_client_get_key($ds_url);
  $verification_token = tmgmt_auth_receiver_controller()->signToken($response['token'], $key);
  $ts_connector = new TMGMTClientTSConnector($translation_server->url);

  try {
    $ts_connector->createTC($verification_token);
  }
  catch (TMGMTRemoteException $e) {
    watchdog('tmgmt_client', 'Create translation client error: %error', array('%error' => $e->getMessage()), WATCHDOG_ERROR);
    form_set_error('', $e->getMessage());
    return;
  }

  try {
    // If we got to this point the TS key should be available for us at the DS.
    // Store it in the form state for further processing in the submit handler.
    $ds_connector->reset();
    $form_state['key_info'] = $ds_connector->getTSKey($translation_server->external_id);
  }
  catch (TMGMTRemoteException $e) {
    watchdog('tmgmt_client', 'Retrieve TS key error: %error', array('%error' => $e->getMessage()), WATCHDOG_ERROR);
    form_set_error('', $e->getMessage());
    return;
  }
}

/**
 * Submit callback to save TS key received in the validate callback.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 */
function tmgmt_client_ds_checkout_negotiate_ts_key_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  /**
   * @var TMGMTJob $job
   */
  $job = $form_state['tmgmt_job'];

  tmgmt_auth_receiver_controller()->receive($form_state['key_info'], 'tmgmt_remote_server', $job->getSetting('server_id'));
}

/**
 * Provides a form to select from available TS.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 * @param TMGMTJob $job
 *   Job entity that is being checked out.
 *
 * @return array
 *   Drupal form array.
 */
function tmgmt_client_ds_checkout_select_ts_form($form, &$form_state, TMGMTJob $job) {

  $translator = $job->getTranslator();
  $ds_connector = new TMGMTClientDSConnector($translator->getSetting('ds_url'));

  /* @var TMGMTDefaultRemoteTranslatorPluginController $plugin */
  $plugin = $job->getTranslatorController();
  $search_data = array();
  if (isset($job->settings['search_data'])) {
    $search_data = $job->settings['search_data'];
  }

  /**
   * @var TMGMTRemoteServerController $ts_controller
   */
  $ts_controller = entity_get_controller('tmgmt_remote_server');

  $local_ts_records = $ts_controller->loadAll('ts', 'external_id');
  $result = $ds_connector->getAvailableTS(
    $plugin->mapToRemoteLanguage($translator, $job->source_language), $plugin->mapToRemoteLanguage($translator, $job->target_language), $search_data);

  // We need to provide default radio option, else it will throw validation
  // error on plugin switch.
  $options = array('0' => array('name' => '', 'description' => ''));

  foreach ($result['servers'] as $translation_server) {

    if (!isset($local_ts_records[$translation_server['server_id']])) {
      $ts = $ts_controller->create(array(
        'external_id' => $translation_server['server_id'],
        'url' => $translation_server['url'],
        'name' => $translation_server['name'],
        'type' => 'ts',
      ));
      $ts_controller->save($ts);

      $server_id = $ts->server_id;
    }
    else {
      $server_id = $local_ts_records[$translation_server['server_id']]->server_id;
    }

    $options[$server_id] = array(
      'name' => $translation_server['name'],
      'description' => $translation_server['ts_detail_markup'],
    );
  }

  if (!empty($result['metadata']['stylesheet'])) {
    $form['#attached']['css'][] = array(
      'data' => $result['metadata']['stylesheet'],
      'type' => 'inline',
    );
  }

  $form['#attached']['js'][] = drupal_get_path('module', 'tmgmt_client') . '/js/tmgmt_client.job_checkout.js';
  $form['#attached']['css'][] = drupal_get_path('module', 'tmgmt_client') . '/css/tmgmt_client.job_checkout.css';

  $form['search_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search options'),
    '#tree' => TRUE,
  );

  if (isset($result['filter_definition'])) {
    foreach($result['filter_definition'] as $name => $definition) {
      // Override the default value if we have it locally.
      if (isset($search_data[$name])) {
        $definition['default_value'] = $search_data[$name];
      }
      tmgmt_client_build_remote_form_element($form['search_data'], $name, $definition);
    }
  }

  $form['search_data']['search'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#submit' => array('tmgmt_client_ds_checkout_search_server_submit'),
    '#ajax' => array(
      'callback' => 'tmgmt_client_checkout_form_ajax',
      'wrapper' => 'tmgmt-ui-translator-settings',
    ),
  );

  if (count($options) > 1) {
    $form['server_id'] = array(
      '#type' => 'ts_radios',
      '#title' => t('Translation server'),
      '#options' => $options,
      '#description' => t('Please select a translation server to which you want to submit the job.'),
      '#default_value' => $job->getSetting('server_id') != NULL ? $job->getSetting('server_id') : '0',
      '#required' => TRUE,
      '#ajax' => array(
        'callback' => 'tmgmt_client_checkout_select_ts_form_ajax',
        'wrapper' => 'tmgmt-ui-translator-settings',
      ),
      '#prefix' => !empty($result['metadata']['opening_markup']) ? $result['metadata']['opening_markup'] : '',
      '#suffix' => !empty($result['metadata']['closing_markup']) ? $result['metadata']['closing_markup'] : '',
    );
  }
  else {
    $form['no_results_info'] = array(
      '#type' => 'markup',
      '#markup' => t('No translation servers found matching your criteria.')
    );
  }

  return $form;
}

/**
 * Ajax call that handles selection of a TS.
 *
 * This is the actual place where we store server info into the job settings.
 */
function tmgmt_client_checkout_select_ts_form_ajax($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  $ts = entity_load_single('tmgmt_remote_server', $form_state['values']['settings']['server_id']);
  $form_state['server_id'] = $form_state['tmgmt_job']->settings['server_id'] = $form_state['values']['settings']['server_id'];
  $form_state['tmgmt_job']->settings['server_name'] = $ts->name;
  $form_state['tmgmt_job']->save();

  return tmgmt_client_checkout_form_ajax($form, $form_state);
}

/**
 * Helper function to build a form element based on remote definition.
 *
 * @param array $form
 *   Drupal form to which to build the element.
 * @param $name
 *   Element name.
 * @param $definition
 *   Remote definition.
 */
function tmgmt_client_build_remote_form_element(&$form, $name, $definition) {

  $element = array(
    '#title' => $definition['title'],
    '#description' => $definition['description'],
    '#size' => $definition['size'],
    '#default_value' => $definition['default_value']
  );

  switch($definition['type']) {
    case 'enum':
      $element['#type'] = 'select';
      $element['#options'] = $definition['options'];
      $element['#empty_option'] = $definition['empty_option'];
      break;
    case 'varchar':
    case 'numeric':
      $element['#type'] = 'textfield';
      break;
  }

  $form[$name] = $element;
}

/**
 * Runs submit on server search action.
 */
function tmgmt_client_ds_checkout_search_server_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  /**
   * @var TMGMTJob $job$form_state['tmgmt_job']->settings
   */
  $job = $form_state['tmgmt_job'];
  $job->settings['search_data'] = $form_state['values']['settings']['search_data'];
  $job->save();
}

/**
 * Provides form for user input to register and authenticate at DS.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 * @param TMGMTJob $job
 *   Job entity that is being checked out.
 *
 * @return array
 *   Drupal form array.
 */
function tmgmt_client_ds_checkout_authenticate_form($form, &$form_state, TMGMTJob $job) {

  // Determine which authentication action has been selected.
  $auth_action = NULL;
  if (isset($form_state['values']['settings']['auth_action'])) {
    $auth_action = $form_state['values']['settings']['auth_action'];
  }

  // Determine if we have a new registration.
  $new_registration = FALSE;
  if (isset($form_state['new_registration'])) {
    $new_registration = $form_state['new_registration'];
  }

  // DS settings form.
  $form += tmgmt_client_ds_settings_form($form, $form_state, $job->getTranslator());

  // If we have new registration, lead user by switching the auth action
  // and disabling fields that should not be touched at this point.
  if ($new_registration) {
    $form['ds_url']['#disabled'] = TRUE;
  }
  // Display the selection only if not new registration.
  else {
    $form['auth_action'] = array(
      '#type' => 'radios',
      '#title' => t('TMGMT Directory authentication'),
      '#options' => array(
        'login' => t('I already have an account at the TMGMT Directory, I will authenticate with my login and password.'),
        'register' => t('I do not have an account at the TMGMT Directory, need to register.'),
      ),
      '#required' => TRUE,
      '#ajax' => array(
        'callback' => 'tmgmt_client_checkout_form_ajax',
        'wrapper' => 'tmgmt-ui-translator-settings',
      ),
    );
  }

  // If no auth action selected, just return the basic form.
  if (!isset($auth_action)) {
    return $form;
  }

  // We are going for login action.
  if ($auth_action == 'login') {
    if ($new_registration) {
      $form['info'] = array(
        '#markup' => '<div class="messages status">' .
            t('Your account at the TMGMT Directory was created. To finish the registration process we have sent you an email with further instructions. Upon completion please authenticate your web site at the TMGMT Directory using your TMGMT Directory user name and password that can be entered below.') . '</div>'
      );
    }
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('User name'),
      '#required' => TRUE,
      '#description' => t('Enter your TMGMT Directory user name.'),
    );
    $form['pass'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#required' => TRUE,
      '#description' => t('Enter your TMGMT Directory password.'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Authenticate'),
      '#validate' => array('tmgmt_client_ds_checkout_authenticate_tc_at_ds_validate'),
      '#submit' => array('tmgmt_client_ds_checkout_authenticate_tc_at_ds_submit'),
      '#ajax' => array(
        'callback' => 'tmgmt_client_checkout_form_ajax',
        'wrapper' => 'tmgmt-ui-translator-settings',
      ),
    );
  }
  // We need to register first.
  elseif ($auth_action == 'register') {
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('User name'),
      '#required' => TRUE,
      '#description' => t('Enter your user name that will be used to login at the TMGMT Directory.'),
    );
    $form['mail'] = array(
      '#type' => 'textfield',
      '#title' => t('E-mail'),
      '#required' => TRUE,
      '#description' => t('Enter your email address where further instructions to finalize the registration process at the TMGMT Directory will be sent.'),
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Register at TMGMT Directory'),
      '#validate' => array('tmgmt_client_ds_checkout_register_at_ds_validate'),
      '#submit' => array('tmgmt_client_ds_checkout_register_at_ds_submit'),
      '#ajax' => array(
        'callback' => 'tmgmt_client_checkout_form_ajax',
        'wrapper' => 'tmgmt-ui-translator-settings',
      ),
    );
  }

  return $form;
}

/**
 * Saves settings of how to handle job submission workflow.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 */
function tmgmt_client_checkout_service_target_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $translator = $form_state['tmgmt_job']->getTranslator();
  $translator->settings['service_target'] = TMGMT_CLIENT_TARGET_DIRECTORY_SERVER;
  $translator->save();
}

/**
 * Validate callback for TC authentication action at DS.
 *
 * Sends DS user login data together with TC url to DS. In response it gets
 * DS key and stores it the form state for further processing in the submit.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 */
function tmgmt_client_ds_checkout_authenticate_tc_at_ds_validate($form, &$form_state) {
  $settings = $form_state['values']['settings'];
  // Provide UUID as own url without the trailing slash.
  $uuid = rtrim(url('', array('absolute' => TRUE)), '/');
  $ds_connector = new TMGMTClientDSConnector($settings['ds_url']);
  $response = NULL;

  try {
    $form_state['key_info'] = $ds_connector->createTC($settings['name'], $settings['pass'], $uuid);
  }
  catch (TMGMTRemoteException $e) {
    form_set_error('', $e->getMessage());
  }
}

/**
 * Stores DS key received by service call in the validate callback.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 */
function tmgmt_client_ds_checkout_authenticate_tc_at_ds_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $settings = $form_state['values']['settings'];

  // Create remote server if not exists.
  /**
   * @var TMGMTRemoteServerController $remote_server_controller
   */
  $remote_server_controller = entity_get_controller('tmgmt_remote_server');
  $remote_server = $remote_server_controller->loadByUrl($settings['ds_url']);
  if (empty($remote_server)) {
    $remote_server = $remote_server_controller->create(array(
      'url' => $settings['ds_url'],
      'type' => 'ds',
      'name' => t('TMGMT Directory'),
    ));
    $remote_server_controller->save($remote_server);
  }

  // Receive DS credentials and save DS url into translator settings.
  // In case of an exception we do not handle it as we cannot. However
  // this status should not occur during regular UCs.

  tmgmt_auth_receiver_controller()->receive($form_state['key_info'], 'tmgmt_remote_server', $remote_server->server_id, TRUE);
  // If we have the ds key, save ds_url into settings.
  $translator = $form_state['tmgmt_job']->getTranslator();
  $translator->settings['ds_url'] = $settings['ds_url'];
  $translator->settings['ds_send_job_data'] = $settings['ds_send_job_data'];
  $translator->save();

}

/**
 * Validator for user DS registration operation.
 *
 * It sends the registration request to the DS.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 *
 * @see tmgmt_client_checkout_settings_register_at_ds_form_submit()
 */
function tmgmt_client_ds_checkout_register_at_ds_validate($form, &$form_state) {
  $settings = $form_state['values']['settings'];
  $ds_connector = new TMGMTClientDSConnector($settings['ds_url']);

  try {
    $ds_connector->registerDSUser($settings['mail'], $settings['name']);
  }
  catch (TMGMTRemoteValidationException $e) {
    form_set_error('', $e->getMessage());
  }
}

/**
 * Submit for user DS registration operation.
 *
 * It only saves the ds_send_job_data setting and sets workflow flags into
 * form state. All the registration logic is in the validate callback.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 *
 * @see tmgmt_client_ds_config_authenticate_form()
 */
function tmgmt_client_ds_checkout_register_at_ds_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  // Save user input.
  $translator = $form_state['tmgmt_job']->getTranslator();
  $translator->settings['ds_send_job_data'] = $form_state['values']['settings']['ds_send_job_data'];
  $translator->save();

  // Set flags to control workflow in tmgmt_client_ds_checkout_authenticate_form()
  $form_state['values']['settings']['auth_action'] = 'login';
  $form_state['new_registration'] = TRUE;
}

/**
 * Ajax callback to switch DS and TS forms in the translator form settings.
 *
 * @param array $form
 *   Drupal form array.
 * @param $form_state
 *   Drupal form state array.
 *
 * @return array
 *   Drupal form array part.
 */
function tmgmt_client_services_target_ajax($form, &$form_state) {
  return $form['plugin_wrapper']['settings'];
}

/**
 * Ajax callback to return checkout settings form part during job checkout.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 *
 * @return array
 *   Drupal form part.
 */
function tmgmt_client_checkout_form_ajax($form, &$form_state) {
  return $form['translator_wrapper']['settings'];
}

/**
 * Implements hook_ID_form_alter().
 *
 * Adds div container for settings form part for ajax call.
 */
function tmgmt_client_form_tmgmt_translator_form_alter(&$form, &$form_state) {
  if (empty($form_state['translator']) || $form_state['translator']->plugin != 'drupal') {
    return;
  }

  // Add settings html element wrapper so we can use ajax calls on it.
  $form['plugin_wrapper']['settings']['#prefix'] = '<div id="plugin-settings-wrapper">';
  $form['plugin_wrapper']['settings']['#suffix'] = '</div>';

  // Push into beginning of submit callbacks queue a callback that will
  // alter form state values.
  array_unshift($form['#submit'], 'tmgmt_client_translator_settings_form_submit');
}

/**
 * Additional submit handler for translator settings page.
 *
 * Purpose is to alter form state values before they arrive to the default
 * submit handler.
 *
 * @param array $form
 *   Drupal form array.
 * @param array $form_state
 *   Drupal form state array.
 */
function tmgmt_client_translator_settings_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $translator = $form_state['translator'];

  $service_target = $form_state['values']['settings']['service_target'];
  $settings = &$form_state['values']['settings'];

  // We want to keep all not overwritten values in the settings so set them
  // into form state to be saved by the default submit handler.
  if (!empty($translator->settings)) {
    foreach ($translator->settings as $key => $setting) {
      if (!isset($settings[$key])) {
        $settings[$key] = $setting;
      }
    }
  }

  $pub = $private = $url = NULL;

  if ($service_target == TMGMT_CLIENT_TARGET_TRANSLATION_SERVER) {
    $pub = &$settings['ts_pub'];
    $private = &$settings['ts_private'];
    $url = $settings['ts_url'];
    $type = 'ts';
  }
  elseif ($service_target == TMGMT_CLIENT_TARGET_DIRECTORY_SERVER) {
    $pub = &$settings['ds_pub'];
    $private = &$settings['ds_private'];
    $url = $settings['ds_url'];
    $type = 'ds';
  }

  $remote_server_controller = tmgmt_client_remote_server_controller();
  $receiver_controller = tmgmt_auth_receiver_controller();

  $remote_server = $remote_server_controller->loadByUrl($url);
  $remote_server_id = $translator->getSetting('remote_server_id');

  // In case we have empty remote_server we have new url so server is going
  // to be changed/created.
  if (empty($remote_server)) {
    // Remove old remote server if present. This will also destroy keys for
    // the old remote server.
    if (!empty($remote_server_id)) {
      $old_remote_server = entity_load_single('tmgmt_remote_server', $remote_server_id);
    }
    if (!empty($old_remote_server)) {
      $remote_server_controller->delete(array($old_remote_server->server_id));
    }

    // Create new remote server with new url.
    $remote_server = $remote_server_controller->create(array(
      'url' => $url,
      'type' => $type,
    ));
    $remote_server_controller->save($remote_server);
  }

  // Load existing key.
  $existing_key = $receiver_controller->loadByEntity('tmgmt_remote_server', $remote_server->server_id);

  // We need this condition to not replace the key each time the settings form
  // is submitted but the case when it should be replaced.
  if (
    // In case we have an existing key but values from user input are different
    // we want to replace it.
    (!empty($existing_key) && ($existing_key->pub != $pub || $existing_key->private != $private))
    ||
    // If there is no key present, create new one.
    empty($existing_key)
  ) {

    $key_info = array(
      'pub' => $pub,
      'private' => $private,
      'created' => time(),
      'expires' => 0,
    );
    $receiver_controller->receive($key_info, 'tmgmt_remote_server', $remote_server->server_id, TRUE);
  }
  // Unset pub and private key values as we do not want them in translator
  // settings.
  unset($pub);
  unset($private);

  // However we want the remote_server_id in settings to be aware of it.
  $settings['remote_server_id'] = $remote_server->server_id;
}

/**
 * Translation server settings form.
 */
function tmgmt_client_ts_settings_form($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {

  // Load existing remote server if any.
  $ts_url = $translator->getSetting('ts_url');
  if (!empty($ts_url)) {
    $remote_server = tmgmt_client_remote_server_controller()->loadByUrl($ts_url);
  }

  if (!empty($remote_server)) {
    $key = tmgmt_auth_receiver_controller()->loadByEntity('tmgmt_remote_server', $remote_server->server_id);
  }
  else {
    $form['initial_info'] = array(
      '#markup' => '<div class="messages warning">' . t('Please provide a Translation Server URL and credentials.') . '</div>'
    );
  }

  $form_state['translator'] = $translator;

  $form['ts_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Server URL'),
    '#default_value' => $ts_url,
    '#description' => t('Please enter the URL of the translation server.'),
    '#element_validate' => array('tmgmt_client_element_validate_url'),
    '#required' => TRUE,
    '#disabled' => $busy,
  );

  $form['ts_pub'] = array(
    '#type' => 'textfield',
    '#title' => t('Public key'),
    '#default_value' => !empty($key) ? $key->pub : NULL,
    '#description' => t('Please enter your public key.'),
    '#element_validate' => array('tmgmt_client_element_validate_api_key'),
    '#required' => TRUE,
    '#disabled' => $busy,
  );

  $form['ts_private'] = array(
    '#type' => 'textfield',
    '#title' => t('Private key'),
    '#default_value' => !empty($key) ? $key->private : NULL,
    '#description' => t('Please enter your private key.'),
    '#element_validate' => array('tmgmt_client_element_validate_api_key'),
    '#required' => TRUE,
    '#disabled' => $busy,
  );

  return $form;
}

/**
 * TMGMT Directory settings form.
 */
function tmgmt_client_ds_settings_form($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE, $keys_config = FALSE) {

  if (isset($form_state['values']['settings']['ds_url'])) {
    $ds_url = $form_state['values']['settings']['ds_url'];
    $ds_send_job_data = $form_state['values']['settings']['ds_send_job_data'];
  }
  // Load DS settings.
  else {
    $ds_url = $translator->getSetting('ds_url');
    $ds_send_job_data = $translator->getSetting('ds_send_job_data');
  }
  // Still no value for $ds_url - load defaults.
  if (empty($ds_url)) {
    $ds_url = TMGMT_CLIENT_DEFAULT_DS_URL;
    $ds_send_job_data = 1;
  }

  $form_state['ds_url'] = $ds_url;

  $remote_server = tmgmt_client_remote_server_controller()->loadByUrl($ds_url);
  if (!empty($remote_server)) {
    $key = tmgmt_auth_receiver_controller()->loadByEntity('tmgmt_remote_server', $remote_server->server_id);
  }

  $form['ds_url'] = array(
    '#type' => 'textfield',
    '#title' => t('TMGMT Directory URL'),
    '#default_value' => $ds_url,
    '#description' => t('Please enter the URL of the TMGMT Directory.'),
    '#required' => TRUE,
    '#disabled' => $busy,
  );
  $form['ds_send_job_data'] = array(
    '#type' => 'checkbox',
    '#title' => t('Submit translation data to the TMGMT Directory'),
    '#description' => t('You can control if data sent for translation will be also sent to the TMGMT Directory.'),
    '#default_value' => $ds_send_job_data,
  );

  if ($keys_config) {
    $form['ds_pub'] = array(
      '#type' => 'textfield',
      '#title' => t('Public key'),
      '#default_value' => !empty($key) ? $key->pub : NULL,
      '#description' => t('Please enter your public key.'),
      '#element_validate' => array('tmgmt_client_element_validate_api_key'),
      '#required' => TRUE,
      '#disabled' => $busy,
    );
    $form['ds_private'] = array(
      '#type' => 'textfield',
      '#title' => t('Private key'),
      '#default_value' => !empty($key) ? $key->private : NULL,
      '#description' => t('Please enter your private key.'),
      '#element_validate' => array('tmgmt_client_element_validate_api_key'),
      '#required' => TRUE,
      '#disabled' => $busy,
    );

    // If we have a connection test result, display it.
    if (isset($form_state['connection_test_result'])) {
      if (isset($form_state['connection_test_result']['error'])) {
        $form['test_ds_connection_info'] = array(
          '#markup' => '<div class="messages error">' . $form_state['connection_test_result']['error'] . '</div>',
        );
      }
      elseif (isset($form_state['connection_test_result']['success'])) {
        $form['test_ds_connection_info'] = array(
          '#markup' => '<div class="messages status">' . $form_state['connection_test_result']['success'] . '</div>',
        );
      }
    }

    $form['test_ds_connection'] = array(
      '#type' => 'submit',
      '#value' => t('Test connection'),
      '#submit' => array('tmgmt_client_translator_settings_form_submit', 'tmgmt_client_test_ds_connection_form_submit'),
      '#ajax' => array(
        'callback' => 'tmgmt_client_services_target_ajax',
        'wrapper' => 'plugin-settings-wrapper',
      ),
    );
  }

  return $form;
}

/**
 * Submit callback to do connection test to DS.
 */
function tmgmt_client_test_ds_connection_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  $remote_server = tmgmt_client_remote_server_controller()->loadByUrl($form_state['values']['settings']['ds_url']);

  if (!empty($remote_server)) {
    $connector = new TMGMTClientDSConnector($remote_server->url);

    try {
      $connector->testConnection();
      $form_state['connection_test_result']['success'] = t('Connection test successful.');
      $form_state['connection_test_result']['error'] = NULL;
    }
    catch (Exception $e) {
      $form_state['connection_test_result']['error'] = $e->getMessage();
    }
  }
  else {
    $form_state['connection_test_result']['error'] = t('TMGMT Directory is not configured yet.');
  }
}
