<?php

abstract class TMGMTClientConnector extends TMGMTRemoteConnector {

  function getUserAgent() {
    $path = drupal_get_path('module', 'tmgmt_client') . '/tmgmt_client.info';
    $info = drupal_parse_info_file($path);
    $version = isset($info['version']) ? $info['version'] : 'dev';

    return parent::getUserAgent() . ' tmgmt_client/' . $version;
  }
}

class TMGMTClientTSConnector extends TMGMTClientConnector {

  function authenticate($resource, array $options = array()) {
    $this->addHeader('Authenticate', tmgmt_client_get_auth_token($this->base_url));
  }

  function getEndPoint() {
    return 'api/v1';
  }

  function getLanguagePairs($source_language = NULL) {
    $this->addQueryParam('source_language', $source_language);
    return $this->getResponse('language-pairs');
  }

  function requestTranslation($source_language, $target_language, $items, $job_comment = NULL) {
    $this->addPostParam('source_language', $source_language);
    $this->addPostParam('target_language', $target_language);
    $this->addPostParam('items', $items);
    $this->addPostParam('job_comment', $job_comment);
    return $this->getResponse('translation-job');
  }

  function pullTranslation($id) {
    return $this->getResponse('translation-job/' . $id . '/item');
  }

  function cancelTranslation($reference) {
    return $this->getResponse('translation-job/' . $reference . '/cancel');
  }

  function createTC($verification_token) {
    $this->addPostParam('verification_token', $verification_token);
    return $this->getResponse('remote-clients');
  }
}

/**
 * Connector class to perform connection to the TMGMT Directory.
 */
class TMGMTClientDSConnector extends TMGMTClientConnector {

  /**
   * Gets available languages from TMGMT Directory for a given source language.
   *
   * @param $source_language
   *   Language code of source text.
   *
   * @return array
   *   List of available languages.
   */
  function getLanguagePairs($source_language = NULL) {
    $this->addQueryParam('source_language', $source_language);
    return $this->getResponse('language-pairs');
  }

  function getSupportedLanguages() {
    return $this->getResponse('supported-languages');
  }

  /**
   * Gets list of available Translation Servers for a given language pair.
   *
   * @param $source_language
   *   Language of source text.
   * @param $target_language
   *   Language to which we want to translate.
   * @param array $search_data
   *   Additional search data.
   *
   * @return array
   *   List of available Translation Servers.
   */
  function getAvailableTS($source_language, $target_language, $search_data = array()) {
    $search_data['source_language'] = $source_language;
    $search_data['target_language'] = $target_language;
    $this->addPostParam('search_data', $search_data);
    return $this->getResponse('translation-servers/filter');
  }

  /**
   * Sends request to register a user account at DS of a type 'client'.
   *
   * @param string $mail
   *   User email address.
   * @param string $name
   *   User name.
   *
   * @return array
   *   Status response.
   */
  function registerDSUser($mail, $name = NULL) {
    $this->addPostParam('mail', $mail);
    $this->addPostParam('name', $name);
    $this->addPostParam('type', 'client');
    return $this->getResponse('ds-users');
  }

  /**
   * Sends request to create a client entity at DS.
   *
   * @param string $name
   *   Existing user name.
   * @param $pass
   *   User account password.
   * @param $uuid
   *   Give TC a chance to provide its UUID.
   *
   * @return array
   *   Response containing DS key.
   */
  function createTC($name, $pass, $uuid) {
    $this->addPostParam('name', $name);
    $this->addPostParam('pass', $pass);
    $this->addPostParam('uuid', $uuid);
    return $this->getResponse('ds-users/translation-client-create');
  }

  /**
   * Initiates credentials negotiation process.
   *
   * @param int $server_id
   *   DS server id for which to start negotiation.
   *
   * @return array
   *   Token in the auth_string format to be signed and sent to the TS.
   */
  function createVerificationToken($server_id) {
    $this->addPostParam('server_id', $server_id);
    return $this->getResponse('verification-tokens');
  }

  /**
   * Pull request to get TS key.
   *
   * @param int $server_id
   *   DS server id for which to download key.
   *
   * @return array
   *   TS key as a auth_string.
   */
  function getTSKey($server_id) {
    return $this->getResponse('transfer-keys/' . $server_id);
  }

  /**
   * Does request to test connection with DS.
   *
   * @return NULL
   *   Empty response will arrive.
   */
  function testConnection() {
    $this->addPostParam('test_data', 'test_value');
    return $this->getResponse('ds-users/test-connection');
  }

  /**
   * Sends info of a job that has been submitted to TS to the TMGMT Directory.
   *
   * @param TMGMTJob $job
   *   Job for which we want to send info.
   * @param int $action
   *   Action that has been performed over the job.
   * @param string $note
   *   Custom data to be attached.
   */
  static function sendJobInfo(TMGMTJob $job, $action, $note = NULL) {

    if ($job->getTranslator()->getSetting('service_target') != TMGMT_CLIENT_TARGET_DIRECTORY_SERVER) {
      return;
    }

    $instance = new TMGMTClientDSConnector($job->getTranslator()->getSetting('ds_url'));

    $this_plugin = $job->getTranslatorController();
    $translator = $job->getTranslator();

    $job_data = array(
      'reference' => $job->reference,
      'source_language' => $this_plugin->mapToRemoteLanguage($translator, $job->source_language),
      'target_language' => $this_plugin->mapToRemoteLanguage($translator, $job->target_language),
      'job_word_count' => $job->getWordCount(),
      'job_state' => $job->getState(),
      'job_items_count' => count($job->getItems()),
      'data' => $job->getTranslator()->getSetting('ds_send_job_data') ? $job->getData() : array(),
    );

    $ts = entity_load_single('tmgmt_remote_server', $job->getSetting('server_id'));

    $instance->addPostParam('job_data', $job_data);
    $instance->addPostParam('server_id', $ts->external_id);
    $instance->addPostParam('action', $action);
    $instance->addPostParam('origin', TMGMT_DS_JOB_ACTION_ORIGIN_CLIENT);
    $instance->addPostParam('note', $note);

    try {
      $instance->getResponse('job-messages');
    }
      // Silently catch all errors so that user will not get interrupted by
      // DS errors.
    catch (Exception $e) {
      watchdog_exception('tmgmt_ds_job_message', $e);
    }
  }

  function authenticate($resource, array $options = array()) {
    $this->addHeader('Authenticate', tmgmt_client_get_auth_token($this->base_url));
  }

  function getEndPoint() {
    return 'api/v1';
  }

}
