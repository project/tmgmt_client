<?php
/**
 * @file
 *   tmgmt_client entities.
 */

/**
 * Entity to represent a remote server.
 */
class TMGMTRemoteServer extends Entity {
  public $server_id;
  public $external_id;
  public $type;
  public $url;
  public $name;
}

/**
 * Overrides the base TMGMTJob entity to do DS calls on workflow actions over
 * the translation job.
 */
class TMGMTClientJob extends TMGMTJob {

  public function finished($message = NULL, $variables = array(), $type = 'status') {
    parent::finished($message, $variables, $type);
    if (!empty($this->settings['server_id'])) {
      TMGMTClientDSConnector::sendJobInfo($this, TMGMT_DS_JOB_ACTION_TRANSLATION_FINISHED);
    }
  }

  public function cancelled($message = NULL, $variables = array(), $type = 'status') {
    parent::cancelled($message, $variables, $type);
    if (!empty($this->settings['server_id'])) {
      TMGMTClientDSConnector::sendJobInfo($this, TMGMT_DS_JOB_ACTION_TRANSLATION_CANCELED);
    }
  }

  public function rejected($message = NULL, $variables = array(), $type = 'error') {
    parent::rejected($message, $variables, $type);
    if (!empty($this->settings['server_id'])) {
      TMGMTClientDSConnector::sendJobInfo($this, TMGMT_DS_JOB_ACTION_TRANSLATION_REJECTED);
    }
  }

  function defaultLabel() {
    $label = parent::defaultLabel();

    if ($server_name = $this->getSetting('server_name')) {
      $label .= ' (' . check_plain($server_name) . ')';
    }

    return $label;
  }
}
