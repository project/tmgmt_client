<?php
/**
 * @file
 *   tmgmt_client ui translator controller.
 */

/**
 * Default remote translation plugin controller.
 */
class TMGMTDrupalTranslatorUIController extends TMGMTDefaultTranslatorUIController {

  /**
   * Overrides TMGMTDefaultTranslatorUIController::pluginSettingsForm().
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {

    $form = parent::pluginSettingsForm($form, $form_state, $translator, $busy);

    // Load translator from db as all the values will get wiped out when
    // switching between service targets due to tmgmt_translator_form() where
    // translator settings get repopulated from form state.
    if (!empty($translator->name)) {
      $_translator = tmgmt_translator_load($translator->name);
    }

    // No translator created yet, use the provided one.
    if (empty($_translator)) {
      $_translator = $translator;
    }

    $form_state['translator'] = $_translator;

    $service_target = NULL;
    if (isset($form_state['values']['settings']['service_target'])) {
      $service_target = $form_state['values']['settings']['service_target'];
    }
    else {
      $service_target = $_translator->getSetting('service_target');
    }

    $form['service_target'] = array(
      '#type' => 'radios',
      '#title' => t('Language Service Provider'),
      '#options' => array(
        TMGMT_CLIENT_TARGET_TRANSLATION_SERVER => t('<strong>Translation server</strong> -  jobs will be submitted directly to a translation server of your choice. You will need the corresponding URL and Key Set to connect.'),
        TMGMT_CLIENT_TARGET_DIRECTORY_SERVER => t('<strong>TMGMT Directory</strong> - when submitting a job you will be presented with a list of available translation servers from which to choose.'),
      ),
      '#default_value' => $service_target,
      // TODO - we need to update the documentation link.
      '#description' => t('Please refer to the <a href="@url">documentation</a> for more details.',
        array('@url' => url(''))),
      '#ajax' => array(
        'callback' => 'tmgmt_client_services_target_ajax',
        'wrapper' => 'plugin-settings-wrapper',
      ),
      '#required' => TRUE,
      '#disabled' => $busy,
    );

    if ($service_target == TMGMT_CLIENT_TARGET_DIRECTORY_SERVER) {
      $form += tmgmt_client_ds_settings_form($form, $form_state, $_translator, $busy, TRUE);
    }
    elseif ($service_target == TMGMT_CLIENT_TARGET_TRANSLATION_SERVER) {
      $form += tmgmt_client_ts_settings_form($form, $form_state, $_translator, $busy);
    }
    else {
      $form['initial_info'] = array(
        '#markup' => '<div class="messages warning">' . t('Before proceeding to the service configuration, please select the way you wish to target a translation service.') . '</div>'
      );
    }

    return $form;
  }

  /**
   * Implements TMGMTTranslatorUIControllerInterface::checkoutSettingsForm().
   */
  public function checkoutSettingsForm($form, &$form_state, TMGMTJob $job) {

    // Make job available for all validate and submit handlers.
    $form_state['tmgmt_job'] = $job;

    $translator = $job->getTranslator();
    $service_target = $translator->getSetting('service_target');


    if ($service_target == TMGMT_CLIENT_TARGET_DIRECTORY_SERVER) {
      $form += tmgmt_client_ds_checkout_form($form, $form_state, $job);
    }
    elseif ($service_target == TMGMT_CLIENT_TARGET_TRANSLATION_SERVER) {
      $form['job_comment'] = array(
        '#type' => 'textarea',
        '#title' => t('Job comment'),
        '#description' => t('Provide additional info or instructions which can be useful for the translator.'),
        '#default_value' => $job->getSetting('job_comment'),
      );
    }
    elseif (empty($service_target)) {
      $form['initial_info'] = array(
        '#markup' => '<div class="messages warning">' .
            t('Before proceeding, you can go for assisted setup that will be done by the TMGMT Directory or you can ' .
                '<a href="@url">configure the @label translator</a> to directly use its service.',
              array(
                '@url' => url('admin/config/regional/tmgmt/translators/manage/' . $translator->name),
                '@label' => $translator->label,
              )
            ) . '</div>'
      );
      $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Use assisted setup'),
        '#ajax' => array(
          'callback' => 'tmgmt_client_checkout_form_ajax',
          'wrapper' => 'tmgmt-ui-translator-settings',
        ),
        '#submit' => array('tmgmt_client_checkout_service_target_submit'),
      );
    }

    return parent::checkoutSettingsForm($form, $form_state, $job);
  }

  public function checkoutInfo(TMGMTJob $job) {

    $server_id = $job->getSetting('server_id');

    // Nothing to do.
    if (!$server_id) {
      return;
    }

    $ts = entity_load_single('tmgmt_remote_server', $job->getSetting('server_id'));
    $ds_url = $job->getTranslator()->getSetting('ds_url');
    $remote_server = tmgmt_client_remote_server_controller()->loadByUrl($ds_url);

    if (!empty($remote_server)) {
      $key = tmgmt_auth_receiver_controller()->loadByEntity('tmgmt_remote_server', $remote_server->server_id);
    }

    // Add rating widget only if we have server_id => the job has been submitted
    // through DS and we have DS key.
    if (empty($ts) || empty($key)) {
      return NULL;
    }

    $info['server_info'] = array(
      '#type' => 'item',
      '#title' => check_plain($ts->name),
      '#markup' => l($ts->url, $ts->url),
    );

    if ($job->state == TMGMT_JOB_STATE_FINISHED) {
      $auth_token = tmgmt_auth_receiver_controller()->createAuthToken($key);

      $src = $ds_url . '/rating-widget/tmgmt_ds_job/';
      $src .= $job->reference . '/';
      $src .= $ts->external_id . '/';
      $src .= $auth_token;

      $info['rating_widget'] = array(
        '#markup' => '<iframe src="' . $src . '" width="98%" height="400" style="border: 1px solid silver; padding: 10px;"></iframe>',
      );
    }

    return $info;
  }

}
