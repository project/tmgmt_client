<?php

/**
 * Default (base) implementation for translator plugins that connect with remote
 * translation services.
 */
class TMGMTDefaultRemoteTranslatorPluginController extends TMGMTDefaultTranslatorPluginController {

  /**
   * Overrides TMGMTPluginBase::__construct().
   */
  public function __construct($type, $plugin) {
    parent::__construct($type, $plugin);
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::checkoutInfo().
   */
  public function hasCheckoutSettings(TMGMTJob $job) {
    return TRUE;
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::getSupportedRemoteLanguages().
   */
  function getSupportedRemoteLanguages(TMGMTTranslator $translator) {
    $available_languages = array();

    if ($translator->getSetting('service_target') == TMGMT_CLIENT_TARGET_DIRECTORY_SERVER && $ds_url = $translator->getSetting('ds_url')) {
      $ds_connector = new TMGMTClientDSConnector($ds_url);

      try {
        $supported_languages = $ds_connector->getSupportedLanguages();
        $this->initLanguageMappingsFromDS($translator, $supported_languages);

        foreach ($supported_languages as $code => $supported_language) {
          $available_languages[$code] = $code;
        }
      }
      catch (TMGMTRemoteException $e) {
        watchdog_exception('tmgmt_client', $e);
      }
    }
    elseif ($translator->getSetting('service_target') == TMGMT_CLIENT_TARGET_TRANSLATION_SERVER && $ts_url = $translator->getSetting('ts_url')) {
      $connector = new TMGMTClientTSConnector($ts_url);

      try {
        foreach ($connector->getLanguagePairs() as $lang_pair) {
          $available_languages[$lang_pair['source_language']] = $lang_pair['source_language'];
          $available_languages[$lang_pair['target_language']] = $lang_pair['target_language'];
        }
      }
      catch (TMGMTRemoteException $e) {
        watchdog('tmgmt_client', $e);
      }
    }

    return $available_languages;
  }

  /**
   * Will create mappings to DS supported languages.
   *
   * @param TMGMTTranslator $translator
   *   TMGMT Translator.
   * @param $ds_supported_languages
   *   Languages supported by DS.
   */
  protected function initLanguageMappingsFromDS(TMGMTTranslator $translator, $ds_supported_languages) {
    $local_languages = language_list();
    $settings_updated = FALSE;
    $site_default_country = variable_get('site_default_country');
    $matched_by_country = NULL;

    foreach ($local_languages as $code => $info) {
      if (isset($ds_supported_languages[$code . '-' . $site_default_country])) {
        $matched_by_country = $code;
        $translator->settings['remote_languages_mappings'][$code] = $code . '-' . $site_default_country;
        $settings_updated = TRUE;
      }
    }

    foreach ($ds_supported_languages as $code => $info) {
      $alias = $info['alias'];

      // If have an alias which is not yet among remote languages mappings
      // we add it up.
      if (
        // If we have an alias and
        !empty($alias) &&
        // The alias has not been set yet or is set to the same value (can
        // happen if the form is saved without successfully getting remote
        // languages).
        (empty($translator->settings['remote_languages_mappings'][$alias]) || $translator->settings['remote_languages_mappings'][$alias] == $alias) &&
        $alias != $matched_by_country
      ) {
        $translator->settings['remote_languages_mappings'][$alias] = $code;
        $settings_updated = TRUE;
      }
    }

    if ($settings_updated) {
      $translator->save();
    }
  }

  /**
   * Builds the translation request array for a job item in a translation
   * request array.
   *
   * @param $item
   *   The job item to generate the request array for.
   *
   * @return array
   *   An array representing the translation request data for the passed job
   *   item.
   */
  public function getTranslationRequestItemArray(TMGMTJobItem $item) {
    $data = array(
      'data' => $this->getTranslationRequestItemData($item),
      'label' => $item->getSourceLabel(),
      'callback' => $this->getCallbackUrl($item),
    );

    // Notify TS about DS only in case service target is DS.
    if ($item->getTranslator()->getSetting('service_target') == TMGMT_CLIENT_TARGET_DIRECTORY_SERVER) {
      $data['ds_url'] = $item->getTranslator()->getSetting('ds_url');
    }

    return $data;
  }

  /**
   * Retrieves the filtered and structured data array for a single job item in
   * a translation request array.
   *
   * @param $item
   *   The job item to retrieve the structured data array for.
   *
   * @return array
   *   The structured data array for the passed job item.
   */
  public function getTranslationRequestItemData(TMGMTJobItem $item) {
    $filtered = array_filter(tmgmt_flatten_data($item->getData()), '_tmgmt_filter_data');

    // Return a filtered, unflattened data structure.
    return tmgmt_unflatten_data($filtered);
  }

  /**
   * Retrieve the callback url for a job item.
   */
  public function getCallbackUrl(TMGMTJobItem $item) {
    return url('tmgmt-' . str_replace('_', '-', $this->pluginType) . '-callback/' . $item->tjiid, array('absolute' => TRUE));
  }

  /**
   * Implements TMGMTTranslatorPluginControllerInterface::requestTranslation().
   */
  public function requestTranslation(TMGMTJob $job) {

    $items = array();
    foreach ($job->getItems() as $item) {
      $items[$item->tjiid] = $this->getTranslationRequestItemArray($item);
    }

    $translator = $job->getTranslator();

    if ($translator->getSetting('service_target') == TMGMT_CLIENT_TARGET_TRANSLATION_SERVER) {
      $server_id = $translator->getSetting('remote_server_id');
    }
    else {
      $server_id = $job->getSetting('server_id');
    }

    if (empty($server_id)) {
      $job->rejected('No Translation server selected.');
      return;
    }

    $translation_server = entity_load_single('tmgmt_remote_server', $server_id);
    $connector = new TMGMTClientTSConnector($translation_server->url);

    try {
      $response = $connector->requestTranslation(
        $translator->mapToRemoteLanguage($job->source_language),
        $translator->mapToRemoteLanguage($job->target_language),
        $items,
        $job->getSetting('job_comment')
      );

      if (isset($response['reference'])) {
        $job->reference = $response['reference'];
      }

      TMGMTClientDSConnector::sendJobInfo($job, TMGMT_DS_JOB_ACTION_SUBMITTED_TO_TS);

      if ($job->getSetting('job_comment')) {
        $job->addMessage('Provided job comment: @comment', array('@comment' => $job->getSetting('job_comment')));
      }
      // The job has been submitted successfully.
      $job->submitted();

      if (!empty($response['data']) && is_array($response['data'])) {
        // Process the response from the translation service.
        foreach ($response['data'] as $key => $item) {
          if (is_array($item)) {
            // In some cases the translation might already be finished.
            $this->processTranslatedData(tmgmt_job_item_load($key), $item);
          }
        }
      }

    }
    catch (TMGMTRemoteConnectionException $e) {
      $job->rejected($e->getMessage());
    }
    catch (TMGMTRemoteValidationException $e) {
      $job->rejected($e->getMessage());
    }

  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::cancelTranslation().
   */
  public function cancelTranslation(TMGMTJob $job) {
    // We can't cancel a job that we don't know the remote reference of.
    if (!isset($job->reference)) {
      return FALSE;
    }

    $translation_server = entity_load_single('tmgmt_remote_server', $job->getSetting('server_id'));
    $ts_connector = new TMGMTClientTSConnector($translation_server->url);

    try {
      $ts_connector->cancelTranslation($job->reference);
      $job->cancelled();
      return TRUE;
    }
    catch (TMGMTRemoteException $e) {
      $job->addMessage($e->getMessage(), array(), 'error');
      return FALSE;
    }

  }

  /**
   * Saves translated data in a job item.
   */
  public function processTranslatedData(TMGMTJobItem $item, $data) {
    $translation = array();
    foreach (tmgmt_flatten_data($data) as $path => $value) {
      if (isset($value['#translation']['#text'])) {
        $translation[$path]['#text'] = $value['#translation']['#text'];
      }
    }

    $item->addTranslatedData(tmgmt_unflatten_data($translation));
  }

  public function canTranslate(TMGMTTranslator $translator, TMGMTJob $job) {
    if ($translator->getSetting('service_target') == TMGMT_CLIENT_TARGET_TRANSLATION_SERVER) {
      $remote_server = tmgmt_client_remote_server_controller()->loadByUrl($translator->getSetting('ts_url'));
      if (!empty($remote_server)) {
        $key = tmgmt_auth_receiver_controller()->loadByEntity('tmgmt_remote_server', $remote_server->server_id);
      }

      if (!empty($key)) {
        return array_key_exists($translator->mapToRemoteLanguage($job->target_language),
          $this->getSupportedTargetLanguages($translator, $job->source_language));
      }
    }
    elseif (
      $translator->getSetting('service_target') == TMGMT_CLIENT_TARGET_DIRECTORY_SERVER &&
      $translator->getSetting('ds_url') != NULL
    ) {
      return array_key_exists($translator->mapToRemoteLanguage($job->target_language),
        $this->getSupportedTargetLanguages($translator, $job->source_language));
    }
    else {
      return TRUE;
    }
  }

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::getSupportedTargetLanguages().
   */
  public function getSupportedTargetLanguages(TMGMTTranslator $translator, $source_language) {

    if ($translator->getSetting('service_target') == TMGMT_CLIENT_TARGET_DIRECTORY_SERVER) {
      $connector = new TMGMTClientDSConnector($translator->getSetting('ds_url'));

      // Check if we have remote mappings. If not, try to map languages from DS.
      // This is necessary as the job checkout form would give unsupported
      // languages most of the time.
      if (empty($translator->settings['remote_languages_mappings'])) {
        $this->initLanguageMappingsFromDS($translator, $connector->getSupportedLanguages());
        $connector->reset();
      }

    }
    elseif ($translator->getSetting('service_target') == TMGMT_CLIENT_TARGET_TRANSLATION_SERVER) {
      $connector = new TMGMTClientTSConnector($translator->getSetting('ts_url'));
    }
    else {
      return array();
    }

    $available_languages = array();

    try {
      foreach ($connector->getLanguagePairs($translator->mapToRemoteLanguage($source_language)) as $lang_pair) {
        $available_languages[$lang_pair['target_language']] = $lang_pair['target_language'];
      }
    }
    catch (TMGMTRemoteException $e) {
      watchdog_exception('tmgmt_ds', $e);
    }

    return $available_languages;
  }
}

/**
 * Translator plugin controller class for the client translator plugin.
 */
class TMGMTDrupalTranslatorPluginController extends TMGMTDefaultRemoteTranslatorPluginController {

  /**
   * Overrides TMGMTDefaultTranslatorPluginController::isAvailable().
   */
  public function isAvailable(TMGMTTranslator $translator) {
    return TRUE;
  }

  /**
   * Pulls a translation from the server
   */
  public function pullTranslation(TMGMTJobItem $item, $id) {

    $translator = $item->getJob()->getTranslator();

    if ($translator->getSetting('service_target') == TMGMT_CLIENT_TARGET_TRANSLATION_SERVER) {
      $server_id = $translator->getSetting('remote_server_id');
    }
    else {
      $server_id = $item->getJob()->getSetting('server_id');
    }

    $translation_server = entity_load_single('tmgmt_remote_server', $server_id);
    $connector = new TMGMTClientTSConnector($translation_server->url);

    try {
      $response = $connector->pullTranslation($id);
      if (!empty($response)) {
        $this->processTranslatedData($item, $response);
        $item->addMessage('Translation pulled from remote server.');
      }
    }
    catch (Exception $e) {
      $item->addMessage('Unable to pull translation from server: ' . $e->getMessage());
    }

  }
}
